fetch('cars.json')
  .then(response => response.json())
  .then(data => {
    // Create a set to store unique brands
    let brands = new Set();

    // Loop through data to extract brands
    data.forEach(car => {
      brands.add(car.brand);
    });

    // Create a list of groups with hyperlinks
    let groups = '';
    brands.forEach(brand => {
      groups += `<li><a href="#" class="brand" data-brand="${brand}">${brand}</a></li>`;
    });
    document.getElementById('groups').innerHTML = groups;

    // Handle click on a brand link
    let brandLinks = document.querySelectorAll('.brand');
    brandLinks.forEach(link => {
      link.addEventListener('click', e => {
        e.preventDefault();

        // Reset cart to 0
        updateCart([]);

        let brand = link.getAttribute('data-brand');

        // Filter data based on brand
        let filteredData = data.filter(car => car.brand === brand);

        // Create input fields for each car
        let cars = '';
        filteredData.forEach(car => {
          cars += `<div class="car" data-id="${car.id}">${car.brand} ${car.model} (${car.year}) - €${car.price} pro Tag<br><input type="number" class="quantity" value="0" min="0" max="20"> Tag(e)</div>`;
        });
        document.getElementById('cars').innerHTML = cars;

        // Handle change on a quantity input field
        let quantityInputs = document.querySelectorAll('.quantity');
        quantityInputs.forEach(input => {
          input.addEventListener('change', () => {
            let selectedDays = parseInt(input.value);
            if (selectedDays > 20) {
              alert('Sie haben die Anzahl der verfügbaren Tage überschritten. Bitte geben Sie eine Anzahl von Tagen ein, die weniger als 20 beträgt.');
              input.value = 0; // reset the value to 0 if it exceeds the limit
            }
            updateCart(data);
          });
        });
      });
    });
  })
  .catch(error => console.error(error));

function updateCart(data) {
  let cartItems = [];
  let total = 0;
  let cartElements = document.querySelectorAll('.car');

  cartElements.forEach(element => {
    let carId = parseInt(element.getAttribute('data-id'));
    let car = data.find(car => car.id === carId);
    let quantity = parseInt(element.querySelector('.quantity').value);
    if (quantity > 0 && car) {
      let itemPrice = quantity * car.price;
      cartItems.push({
        carId: car.id,
        brand: car.brand,
        model: car.model,
        year: car.year,
        price: car.price,
        quantity: quantity,
        itemPrice: itemPrice
      });
      total += itemPrice;
    }
  });
  
  localStorage.setItem('cart', JSON.stringify(cartItems));

  let cartTotal = document.getElementById('cart-total');
  if (cartTotal) {
    cartTotal.textContent = `€${total.toFixed(2)}`;
  }
}



